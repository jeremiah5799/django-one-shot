from django.forms import ModelForm
from todos.models import TodoList, ToDoItem


class CreateForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]


class CreateItemForm(ModelForm):
    class Meta:
        model = ToDoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]
