from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, ToDoItem
from todos.forms import CreateForm, CreateItemForm


# Create your views here.
def task_list(request):
    tasks = TodoList.objects.all()
    context = {"tasks": tasks}
    return render(request, "tasks/list.html", context)


def details(request, id):
    detail = TodoList.objects.get(id=id)
    context = {
        "detail": detail,
    }
    return render(request, "tasks/detail.html", context)


def create(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("task_list")
    else:
        form = CreateForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


def edit_list(request, id):
    details = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = CreateForm(request.POST, instance=details)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = CreateForm(instance=details)
    context = {
        "details": details,
        "form": form,
    }

    return render(request, "tasks/edit.html", context)


def todo_list_delete(request, id):
    delete_object = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = CreateForm(request.POST, instance=delete_object)
        delete_object.delete()
        return redirect("task_list")
    else:
        form = CreateForm(instance=delete_object)
    context = {
        "delete_object": delete_object,
        "form": form,
    }
    return render(request, "tasks/delete.html", context)


def todo_item_create(request):
    if request.method == "POST":
        form = CreateItemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail")

    else:
        form = CreateItemForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/TodoItem.html", context)


def todo_item_update(request, id):
    specific = ToDoItem.objects.get(id=id)
    if request.method == "POST":
        form = CreateItemForm(request.POST, instance=specific)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail")
    else:
        form = CreateItemForm(instance=specific)
    context = {
        "specific": specific,
        "form": form,
    }
    return render(request, "tasks/update.html", context)
