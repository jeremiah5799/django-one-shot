from django.contrib import admin
from todos.models import TodoList, ToDoItem


# Register your models here.
@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )


@admin.register(ToDoItem)
class ToDoItem(admin.ModelAdmin):
    list_display = (
        "task",
        "due_date",
        "is_completed",
    )
