from django.urls import path
from todos.views import (
    task_list,
    details,
    create,
    edit_list,
    todo_list_delete,
    todo_item_create,
    todo_item_update,
)

urlpatterns = [
    path("", task_list, name="task_list"),
    path("<int:id>/", details, name="todo_list_detail"),
    path("create/", create, name="todo_list_create"),
    path("<int:id>/edit/", edit_list, name="todo_list_update"),
    path("<int:id>/delete/", todo_list_delete, name="todo_list_delete"),
    path("items/create/", todo_item_create, name="todo_item_create"),
    path("items/<int:id>/edit/", todo_item_update, name="todo_item_update"),
]
